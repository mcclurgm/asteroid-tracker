/* model.vala
 *
 * Copyright 2018 Michael McClurg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * NeoModel collects data from NASA's NEO API and presents it in a format that
 * can be used by Vala applications.
 *
 * It makes requests to the API to collect the data.
 * It then collects this information into two Vala objects,
 * {@link Neo} and {@link Approach}.
 * It makes this data available to a client with a number of methods that filter
 * the data.
 * It also has a general method to get all of the approaches, {@link model.get_approaches}.
 *
 * For an MVP, it can only use data for one week at a time (that's a single
 * request to NASA's API).
 * <br>
 * Currently, it can only store data for one year at a time.
 * This is a purely artificial limitation that only serves to make an MVP easier,
 * since the API only allows 7-day time spans.
 * I should be able to lift this eventually, but it'll be a lot of separate requests.
 * <br>
 * Beacuse of this, if you want to change the time scale, you need to create a
 * new Model.
 * Again, this is purely artificial and should be easy enough to make opaque
 * when a client requests a different time span.
 * But for the moment, to make things easy on myself, I'm doing it this way.
 * TODO adjust time span within a single Model instance
*/
class NeoModel {
    Tree<string, Neo> objects;
    List<Approach> approaches;

    /**
     * Returns a list of all approaches in the model.
     */
    public List<Approach> get_approaches () {
        //TODO implement get_approaches
        return null;
    }

    /**
     * Returns a list of all objects in the model.
     */
    public List<Neo> get_objects () {
        //TODO implement get_objects
        return null;
    }

    /**
     * Gets the object with the given name.
     *
     * As far as I know, it should never return multiple objects.
     * These things are named uniquely right?
     * TODO check unique names
     *
     * TODO should this be able to look for multiple objects or should that
     * be left to the client?
     *
     * @param name the name (designation) of the object
     * @return a {@link Neo} with the desired name
     */
    public Neo get_named_object (string name) {
        //TODO implement get_named_object
        return null;
    }

    /**
     * TODO do I need this?
     *
     * Gets all approaches for the objects with the given names.
     *
     * @param name the names (designations) of the objects
     * @return a list of {@link Approach} objects associated with the desired names
     */
    public List<Approach> get_approaches_for_objects (List<string> names) {
        //TODO implement get_approaches_for_objects
        return null;
    }

    /**
     * Gets all approaches made in the given time period.
     *
     * TODO are date parameters optional? I don't think so for my restricted timespans
     * Both parameters are optional. However, at least one must be specified.
     * It is an error to do otherwise.
     * <br>
     * The purpose of this is to allow open-ended ranges: i.e. any time after 10/30/2016.
     * To look for a closed range, simply include values for both parameters.
     *
     * @param after the first date that will be returned
     * @param before the last date that will be returned
     * @return a list of {@link Approach} objects made in the given time span
     */
    public List<Approach> get_approaches_with_date () {
        //TODO implement get_approaches_with_date
        return null;
    }

    /**
     * Gets all approaches made by objects with the given absolute (H) magnitude.
     *
     * Both parameters are optional. However, at least one must be specified.
     * It is an error to do otherwise.
     * <br>
     * The purpose of this is to allow open-ended ranges: i.e. any magnitude
     * greater than 18.9.
     * To look for a closed range, simply include values for both parameters.
     *
     * @param minimum the minimum object magnitude (inclusive) that will be returned
     * @param maximum the maximum object magnitude (inclusive) that will be returned
     * @return a list of {@link Approach} objects made by objects of the given H magnitude
     */
    public List<Approach> get_approaches_with_object_magnitude () {
        //TODO implement get_approaches_with_object_magnitude
        return null;
    }

    /**
     * Gets all approaches that came to a given distance from Earth, in km.
     *
     * Both parameters are optional. However, at least one must be specified.
     * It is an error to do otherwise.
     * <br>
     * The purpose of this is to allow open-ended ranges: i.e. any distance
     * greater than 300000 km.
     * To look for a closed range, simply include values for both parameters.
     *
     * @param minimum the minimum distance (inclusive) in km
     * @param maximum the maximum distance (inclusive) in km
     * @return a list of {@link Approach} objects with the given miss distance
     */
    public List<Approach> get_approaches_with_distance () {
        //TODO implement get_approaches_with_distance
        return null;
    }

    /**
     * Gets all approaches that passed by with relative velocity, in km/s
     * in the given range.
     *
     * Both parameters are optional. However, at least one must be specified.
     * It is an error to do otherwise.
     * <br>
     * The purpose of this is to allow open-ended ranges: i.e. any velocity
     * faster than 20 km/s.
     * To look for a closed range, simply include values for both parameters.
     *
     * @param minimum slowest relative velocity (inclusive) to be returned, in km/s
     * @param maximum fastest relative velocity (inclusive) to be returned, in km/s
     * @return a list of {@link Approach} objects with the given relative velocity
     */
    public List<Approach> get_approaches_with_velocity () {
        //TODO implement get_approaches_with_velocity
        return null;
    }

    /**
     *
     * Gets all approaches made by objects orbiting the given body.
     *
     * Currently, the only option I've seen is Earth.
     * I'll update this as I find more.
     *
     * @param name the name of orbited body
     * @return a list of {@link Approach} objects with the desired orbit
     */
    public List<Approach> get_approaches_with_orbiting_body () {
        //TODO implement get_approaches_with_orbiting_body
        return null;
    }

    /**
     * Gets all approaches made by objects that are considered hazardous.
     *
     * @return a list of {@link Approach} objects made by hazardous objects
     */
    public List<Approach> get_approaches_for_hazardous_objects () {
        // TODO implement get_approaches_for_hazardous_objects
        return null;
    }

    /**
     * Gets all approaches made by objects with estimated diameter in the given range,
     * in meters.
     *
     * Since these values are estimates, NASA gives a range of estimated values.
     * If either extreme estimate is in the specified range,
     * diameter is considered within the range.
     *
     * Both parameters are optional. However, at least one must be specified.
     * It is an error to do otherwise.
     * <br>
     * The purpose of this is to allow open-ended ranges: i.e. any diameter
     * greater than 441 m.
     * To look for a closed range, simply include values for both parameters.
     *
     * @param minimum the minimum object diameter (inclusive) that will be returned, in m
     * @param maximum the maximum object diameter (inclusive) that will be returned, in m
     * @return a list of {@link Approach} objects made by objects of the given H magnitude
     */
    public List<Approach> get_approaches_with_object_diameter () {
        // TODO implement get_approaches_with_object_diameter
        return null;
    }
}

class Neo {
    string reference_id;
    string name;
    double absolute_magnitude_h;
    double min_diameter_meters;
    double max_diameter_meters;
    bool hazardous;

    List<Approach> approaches;
}

class Approach {
    string date_string;
    GLib.Date date;
    int64 epoch_date;
    double velocity_km_per_second;
    double miss_distance_km;
    string orbiting_body;

    Neo approaching_object;
}
