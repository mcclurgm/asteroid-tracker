# Asteroid Tracker

This is a Gtk 3 client for NASA's NEO Watch API, which reports near-Earth approaches of asteroid, comets, and other objects.
It's a project for me to learn Gtk by making a complete application that I can develop over time.

This is a second attempt at [mcclurgm/neo-viewer](https://gitlab.com/mcclurgm/neo-viewer).
In this, I am trying to focus more on design from the start to create a less haphazard project.
